# TPM (Trusted Platform Module)
This project is a set of script to use the TPM on different OS to generate cryptographic secrets like SSH/GPG keys securely hostedin hardware enclave. Those secrets could be use for SSH connection or commit signing

## Ubuntu
### Instructions
The script need to be run before in the order they are listed.

SSH script is to use SSH key for SSH connection, at the moment I did not successfully used TPM stored SSH key to sign commit.
I will deal with GPG once GnuPG 2.3 is released on Ubuntu distribution

#### 1 - Setup TPM
To setup the TPM on your device, follow the instructions:
- Run
```bash
cd ubuntu
bash 1_setup_tpm.sh
```
- If it was the first run of `1_setup_tpm.sh`, disconnect and reconnect so group membership is updated

#### 2- Generate an SSH key in TPM
To generate an SSH key stored in TPM run:
```bash
cd ubuntu
bash 2_ssh_using_tpm.sh
```

#### 3 - Generate GPG key in TPM (Not functional)
TODO once GnuPG 2.3 will be available in Ubuntu repo
```bash
cd ubuntu
bash 3_gpg_using_tpm.sh
```

#### 4 - Use TPM to sign commit (Not functional)
You should only follow one of the following sub sections
##### 4.1 - Use TPM SSH key to sign commit (Not functional)
To use TPM SSH key for git signing, run:
```bash
cd ubuntu
bash 4.1_ssh_tpm_signing.sh
```

##### 4.2 Use TPM GPG key to sign commit (Not functional)
TODO once GnuPG 2.3 will be available in Ubuntu repo
To use TPM GPG key, run:
```bash
cd ubuntu
bash 4.2_gpg_tpm_signing.sh
```

### Sources
- https://wiki.archlinux.org/title/Trusted_Platform_Module#Using_TPM_1.2
- https://github.com/tpm2-software/tpm2-pkcs11/blob/master/docs/INITIALIZING.md
- https://github.com/tpm2-software/tpm2-pkcs11/blob/master/docs/SSH.md
- https://paolozaino.wordpress.com/2021/02/21/linux-configure-and-use-your-tpm-2-0-module-on-linux/
- https://incenp.org/notes/2020/tpm-based-ssh-key.html
- https://gnupg.org/blog/20210315-using-tpm-with-gnupg-2.3.html

## MacOS
### Requirements
- Have installed Secretive
```bash
brew install --cask secretive
```
- Have Secretive initialized (open and configure agent following the prompt)
### Optional
### Instructions
To create and use SSH key stored in T2 chip, run:
```bash
cd macos
bash 1_ssh_using_tpm.sh
```
You will be prompted if you want to use an existing key or if you want to create one (only works for first Secretive SSH key)
### Sources
- https://github.com/maxgoedjen/secretive
