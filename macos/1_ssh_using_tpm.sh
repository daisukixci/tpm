#!/usr/bin/env bash

expected_socket="${HOME}/Library/Containers/com.maxgoedjen.Secretive.SecretAgent/Data/socket.ssh"

read -rp "Do you want us to try generating the SSH key using secretive. (Secretive needs to be initialized, y/n) " choice
case ${choice,,} in
    y|yes)
        # Run the macOS workflow to try our best to generate the ssh key.
        # Seems highly unreliable, worked on my machine but failed on some for no apparent reason
        # If the high rate is too high, just remove the next line and do a documentation to do it
        automator ./Secretive_new_key.workflow
        selected_file=$(cat ~/Desktop/secretive_new_key_path.txt)
        ;;
    *)
        read -rp "Provide path of the SSH public key provided by Secretive " selected_file
esac

git config --global gpg.format ssh
git config --global commit.gpgsign true
git config --global tag.forceSigneAnnoted true
git config --global tag.gpgSign true
git config --global user.signingkey "$selected_file"

case $(basename "$SHELL") in
    bash)
        rc_file="${HOME}/.bashrc"
        ;;
    zsh)
        rc_file="${HOME}/.zshrc"
        ;;
    fish)
        rc_file="${HOME}/.config/fish/config.fish"
        ;;
    *)
        rc_file="${HOME}/.profile"
        ;;
esac

if [[ "$SSH_AUTH_SOCK" != "$expected_socket" ]]; then
    echo "export SSH_AUTH_SOCK=$expected_socket" >> "$rc_file"
fi

ssh_config="${HOME}/.ssh/config"

read -r -p "Do you want the script to try setting up your SSH configuration (y/n)" choice
case ${choice,,} in
    y|yes)
        if grep -q 'Host *' "$ssh_config"; then
            if grep -q IdentityAgent "$ssh_config"; then
                sed -i " " -e "s/^IdentityAgent .*$/IdentityAgent $expected_socket/g" "$ssh_config"
            else
                sed -i " " -e "s/Host \*/Host *\nIdentityAgent $expected_socket/g" "$ssh_config"
            fi
        else
            cat << EOF >> "${HOME}/.ssh/config"
Host *
  IdentityAgent $expected_socket
  AddKeysToAgent yes    # have the ssh command auto-add on first use
  UseKeychain yes       # make --apple-use-keychain and --apple-load-keychain the default
EOF
        fi
        ;;
    *)
        echo "Add one of the following snippet in your $ssh_config according if you want to use the SSH key for all hosts or dedicated hosts only"
            cat << EOF
# By host
Host HOST
  IdentityAgent $expected_socket
  AddKeysToAgent yes    # have the ssh command auto-add on first use
  UseKeychain yes       # make --apple-use-keychain and --apple-load-keychain the default
# For all hosts
Host *
  IdentityAgent $expected_socket
  AddKeysToAgent yes    # have the ssh command auto-add on first use
  UseKeychain yes       # make --apple-use-keychain and --apple-load-keychain the default
EOF
;;
esac

