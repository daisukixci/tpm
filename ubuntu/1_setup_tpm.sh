#!/usr/bin/env bash

# shellcheck source=libs/env.sh
source libs/env.sh

# Setup TPM
sudo apt install ${package_name[*]}
is_active=$(systemctl is-active "$service_name")
if [[ "$is_active" != "active" ]]; then
    sudo systemctl start "$service_name"
    sudo systemctl enable "$service_name"
fi

# Test if user already in TSS group, add it otherwise
if ! id -nG "$user" | grep -q tss; then
    sudo usermod -a -G "$group" "$user"
fi

mkdir -p "$tpm_home"
if ! [[ -d "${tpm_home}/tpm2_pkcs11.sqlite3" ]]; then
    # Init DB
    tpm2_ptool init --path="$tpm_home"
fi
export TPM2_PKCS11_STORE="$tpm_home"
if ! grep -q "export TPM2_PKCS11_STORE=\"$tpm_home\"" "$rc_file" ; then
    echo "export TPM2_PKCS11_STORE=\"$tpm_home\"" >> "$rc_file"
fi
