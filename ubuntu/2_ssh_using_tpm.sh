#!/usr/bin/env bash

# shellcheck source=libs/env.sh
source libs/env.sh

# Setup SSH key
read -r -p "Choose a PIN. (Press ENTER for a randomly generated PIN)" pin
pin=${pin:-$(generate_random 8)}
read -r -p "Choose an ADMIN PIN. (Press ENTER for a randomly generated ADMIN PIN)" admin_pin
admin_pin=${admin_pin:-$(generate_random 8)}
read -r -p "Pick the path for the public key file path. (Press ENTER to accept default $default_pubkey_path)" pubkey_path
pubkey_path=${pubkey_path:-${default_pubkey_path}}
read -r -p "Pick the label for the key stored in TPM. (Press ENTER to accept default $default_label)" label
label=${label:-${default_label}}

#TODO improve the way we detect empty slot
for ((slot=1; slot < 10; slot++)); do
    if [[ $(tpm2_ptool listtokens --pid "$slot" | wc -l) == 0 ]]; then
        echo "Empty token slot found, using it"
        pid="$slot"
        break
    fi
done
if [ -z ${pid+x} ]; then
    echo "No available slot found, exiting"
fi

tpm2_ptool addtoken --pid="$pid" --label="$label" --sopin="$admin_pin" --userpin="$pin" --path="$tpm_home"
tpm2_ptool addkey --label=SSH --userpin="$pin" --algorithm=ecc256 --path="$tpm_home"
ssh-keygen -D ${tpm_lib} > "$pubkey_path"

echo "Public key has been saved in $pubkey_path"
echo "There are two important random numbers for the YubiKey you MUST keep safely."
echo "The User PIN is used during normal operation to authorize an action such as issuing a new GPG signature/SSH connection."
echo "${GREEN}"
echo "***********************************************************"
echo "New User PIN: $pin"
echo "***********************************************************"
echo "${RESET}"
echo "${YELLOW}Please save this new User PIN (copied to clipboard) immediately in your password manager.${RESET}"
echo "$pin" | /usr/bin/xclip -selection clipboard -i
read -rp "${YELLOW}Have you done this?${RESET}"
echo
# TODO Check if below is true
echo "The Admin PIN can be used to reset the PIN if it is ever lost or becomes blocked after the maximum number of incorrect attempts."
echo "${GREEN}"
echo "***********************************************************"
echo "New Admin PIN: $admin_pin"
echo "***********************************************************"
echo "${RESET}"
echo "${YELLOW}Please save this new Admin PIN (copied to clipboard) immediately in your password manager.${RESET}"
echo "$admin_pin" | /usr/bin/xclip -selection clipboard -i
read -rp "${YELLOW}Have you done this? ${RESET}"
echo

read -r -p "Do you want the script to try setting up your SSH configuration (y/n)" choice
case ${choice,,} in
    y|yes)
        if grep -qE '^Host \*$' "$ssh_config"; then
            if grep -q PKCS11Provider "$ssh_config"; then
                sed -i -e "s/^PKCS11Provider.*$/PKCS11Provider $tpm_lib/g" "$ssh_config"
            else
                sed -i -e "s/^Host \*$/Host *\n\tIdentityAgent none/g" "$ssh_config"
                sed -i -e "s:^Host \*$:Host *\n\tPKCS11Provider $tpm_lib:g" "$ssh_config"
            fi
        else
            cat << EOF >> "${HOME}/.ssh/config"
Host *
    PKCS11Provider $tpm_lib
    IdentityAgent none
    IdentitiesOnly no
EOF
        fi
        ;;
    *)
        echo "Add one of the following snippet in your $ssh_config according if you want to use the SSH key for all hosts or dedicated hosts only"
        cat << EOF
# By host
Host HOST
   PKCS11Provider $tpm_lib
   IdentityAgent none
   IdentitiesOnly no
# For all hosts
Host *
   PKCS11Provider $tpm_lib
   IdentityAgent none
   IdentitiesOnly no
EOF
;;
esac
