#!/usr/bin/env bash

function generate_random() {
    size=${1:-8}
    dd if=/dev/urandom bs=1 count="$size" 2>/dev/null | base64 -w 0 | rev | cut -b 2- | rev
}

# Define env
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
RESET=$(tput sgr0)
user="$USER"
tpm_version=$(cat /sys/class/tpm/tpm*/tpm_version_major)
default_pubkey_path="${HOME}/.ssh/ssh_tpm.pub"
default_label="SSH"
ssh_config="${HOME}/.ssh/config"
case $tpm_version in
    2)
        package_name=(
        'tpm2-abrmd'
        'libtpm2-pkcs11-tools'
        'libtpm2-pkcs11-1'
        )
        service_name='tpm2-abrmd'
        group='tss'
        tpm_home="${HOME}/.tpm2_pkcs11"
        tpm_lib='/usr/lib/x86_64-linux-gnu/pkcs11/libtpm2_pkcs11.so'
        ;;
    *)
        echo "Unsupported TPM version"
        exit 0
esac

case $(basename "$SHELL") in
    bash)
        rc_file="${HOME}/.bashrc"
        ;;
    zsh)
        rc_file="${HOME}/.zshrc"
        ;;
    fish)
        rc_file="${HOME}/.config/fish/config.fish"
        ;;
    *)
        rc_file="${HOME}/.profile"
        ;;
esac

export GREEN
export YELLOW
export RESET
export user
export tpm_version
export default_pubkey_path
export default_label
export package_name
export service_name
export group
export tpm_home
export tpm_lib
export rc_file
export ssh_config
