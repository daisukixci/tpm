#!/usr/bin/env bash

# shellcheck source=libs/env.sh
source libs/env.sh

default_gpg_key_id="TODO"

read -r -p "Please provide GPG key ID. (Press ENTER to accept default $default_gpg_key_id)" gpg_key_id
gpg_key_id=${gpg_key_id:-${default_gpg_key_id}}

git config --global commit.gpgsign true
git config --global tag.forceSigneAnnoted true
git config --global tag.gpgSign true
git config --global --unset gpg.format
git config --global user.signingkey "$gpg_key_id"
