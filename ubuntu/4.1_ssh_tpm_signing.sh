#!/usr/bin/env bash

# shellcheck source=libs/env.sh
source libs/env.sh

read -r -p "Pick the path for the public key file path. (Press ENTER to accept default $default_pubkey_path)" pubkey_path
pubkey_path=${pubkey_path:-${default_pubkey_path}}

git config --global commit.gpgsign true
git config --global tag.forceSigneAnnoted true
git config --global tag.gpgSign true
git config --global gpg.format ssh
git config --global user.signingkey "$pubkey_path"
